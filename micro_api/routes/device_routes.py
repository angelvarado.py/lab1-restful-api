from flask import Blueprint, jsonify, request
from logger.logger_base import log

# This class has the capacity to return JSON
class DeviceRoutes(Blueprint):
    def __init__(self, device_service, device_schema):
        super().__init__('device', __name__)
        self.device_service = device_service
        self.device_schema = device_schema
        self.register_routes()

    # Routes
    def register_routes(self):
        self.route('/api/devices', methods=['GET'])(self.get_devices)
        self.route('/api/devices/<int:device_id>', methods=['GET'])(self.get_device_by_id)
        self.route('/api/devices', methods=['POST'])(self.add_device)
        self.route('/api/devices/<int:device_id>', methods=['PUT'])(self.update_device)
        self.route('/api/devices/<int:device_id>', methods=['DELETE'])(self.delete_device)

    # Route for GET
    def get_devices(self):
        try:
            devices = self.device_service.get_all_devices()
            return jsonify(devices), 200
        except Exception as e:
            log.exception(f'Error fetching data from the database: {e}')
            return jsonify({'error': 'Failed to fetch data from the database'}), 500

    # Route for GET BY ID
    def get_device_by_id(self, device_id):
        try:
            device = self.device_service.get_device_by_id(device_id)
            return jsonify(device), 200
        except Exception as e:
            log.exception(f'Error fetching device with id {device_id} from the database: {e}')
            return jsonify({'error': f'Failed to fetch device with id {device_id} from the database'}), 404

    # Route for POST
    def add_device(self):
        req_body = request.get_json()
        if not req_body:
            return jsonify({'error': 'No data in the request'}), 400
        try:
            validated_data = self.device_schema.load(req_body)
            created_device = self.device_service.add_device(validated_data)
            return jsonify(created_device), 201
        
        # The only exception that could happen in add is invalid data because of load method
        except Exception as e:
            log.exception(f'Error adding data to the database: {e}')
            return jsonify({'error': str(e)}), 400

    # Route for PUT
    def update_device(self, device_id):
        req_body = request.get_json()
        if not req_body:
            return jsonify({'error': 'No data in the request'}), 400
        try:
            validated_data = self.device_schema.load(req_body)
            updated_device = self.device_service.update_device(device_id, validated_data)
            return jsonify(updated_device), 201

        # Here, we can have validation exception or no device found exception        
        except Exception as e:
            log.exception(f'Error updating data to the database: {e}')
            return jsonify({'error': str(e)}), 400


    # Route for DELETE
    def delete_device(self, device_id):
        try:
            deleted_device = self.device_service.delete_device(device_id)
            return jsonify(deleted_device), 200
        except Exception as e:
            log.exception(f'Error deleting device with id {device_id} from the database: {e}')
            return jsonify({'error': f'Failed to delete device with id {device_id} from the database'}), 500





