import logging as log

#handles the logger file
# level is set no NOTSET so every log will be register in the log file
log.basicConfig(level=log.NOTSET,
                format='%(asctime)s: %(levelname)s [%(filename)s:%(lineno)s] %(message)s',
                datefmt='%I:%M:%S %p',
                handlers=[
                    log.FileHandler('mongodb_api.log'),
                    log.StreamHandler()
                ] 
)