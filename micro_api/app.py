from flask import Flask
from models.device_models import DeviceModel
from services.device_services import DeviceService 
from routes.device_routes import DeviceRoutes
from schemas.device_schema import DeviceSchema

app = Flask(__name__)

# Create connection to the database 
db_connector = DeviceModel()
db_connector.connect_to_database()

# Associate the services with the data on the database
device_service = DeviceService(db_connector)

# Create validation instance
device_schema = DeviceSchema()

# Associate routes with the services
device_blueprint = DeviceRoutes(device_service, device_schema)
app.register_blueprint(device_blueprint)

# Run application...
if __name__ == '__main__':
    try:
        app.run()
    finally:
        db_connector.close_connection()